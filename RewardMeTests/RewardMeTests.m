//
//  RewardMeTests.m
//  RewardMeTests
//
//  Created by Linda Peckham on 7/9/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MagicalRecord.h"

#import "Task.h"
#import "TaskDetail.h"
#import "Reward.h"
#import "RewardDetail.h"
#import "TaskRewardEvent.h"

@interface RewardMeTests : XCTestCase

@end

@implementation RewardMeTests

- (void)setUp {
    [super setUp];
    
    [MagicalRecord setDefaultModelFromClass:[self class]];
    [MagicalRecord setupCoreDataStackWithInMemoryStore];
}

- (void)tearDown {
    
    [MagicalRecord cleanUp];
    
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
