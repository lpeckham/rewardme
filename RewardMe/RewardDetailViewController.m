//
//  RewardDetailViewController.m
//  RewardMe
//
//  Created by Linda Peckham on 7/15/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "RewardDetailViewController.h"

@interface RewardDetailViewController ()

@end

@implementation RewardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
