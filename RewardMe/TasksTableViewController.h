//
//  FirstViewController.h
//  RewardMe
//
//  Created by Linda Peckham on 7/9/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "Task.h"
#import "TaskDetail.h"
#import "TaskRewardEvent.h"

@interface TasksTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

