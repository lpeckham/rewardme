//
//  TaskRewardDetail.h
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//


@class Task;

@interface TaskDetail : NSManagedObject

@property (nonatomic, retain) NSString * descriptionOfTaskReward;
@property (nonatomic, retain) NSNumber * isRepeatable;
@property (nonatomic, retain) NSNumber * rewardValue;
@property (nonatomic, retain) Task *nameOfTaskReward;

@end
