//
//  RewardDetail.h
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//


@class Reward;

@interface RewardDetail : NSManagedObject

@property (nonatomic, retain) NSNumber * pointsRequiredForReward;
@property (nonatomic, retain) NSString * detailOfReward;
@property (nonatomic, retain) NSNumber * isRepeatable;
@property (nonatomic, retain) Reward *rewards;

@end
