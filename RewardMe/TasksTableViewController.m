//
//  FirstViewController.m
//  RewardMe
//
//  Created by Linda Peckham on 7/9/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "TasksTableViewController.h"

@interface TasksTableViewController ()

@end

@implementation TasksTableViewController
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self createNavigationButtons];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createNavigationButtons{
    
}
@end
