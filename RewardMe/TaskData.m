//
//  TaskData.m
//  
//
//  Created by Linda Peckham on 7/13/15.
//
//

#import "TaskData.h"


@implementation TaskData

@dynamic nameOfTask;
@dynamic rewardPoints;
@dynamic isRepeatable;
@dynamic descriptionOfTask;

@end
