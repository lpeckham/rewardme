//
//  TaskRewardDetail.m
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "TaskDetail.h"
#import "Task.h"


@implementation TaskDetail

@dynamic descriptionOfTaskReward;
@dynamic isRepeatable;
@dynamic rewardValue;
@dynamic nameOfTaskReward;

@end
