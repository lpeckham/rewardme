//
//  TaskRewardEvent.h
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

@class /*NSManagedObject,*/ Task;

@interface TaskRewardEvent : NSManagedObject

@property (nonatomic, retain) NSDate * dateTimeOfEvent;
@property (nonatomic, retain) NSNumber * rewardValue;
@property (nonatomic, retain) NSNumber * isATask;
@property (nonatomic, retain) Task *eventTask;
@property (nonatomic, retain) NSManagedObject *eventReward;

@end
