//
//  TaskData.h
//  
//
//  Created by Linda Peckham on 7/13/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TaskData : NSManagedObject

@property (nonatomic, retain) NSString * nameOfTask;
@property (nonatomic, retain) NSNumber * rewardPoints;
@property (nonatomic, retain) NSNumber * isRepeatable;
@property (nonatomic, retain) NSString * descriptionOfTask;

@end
