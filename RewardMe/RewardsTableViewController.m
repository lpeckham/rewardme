//
//  SecondViewController.m
//  RewardMe
//
//  Created by Linda Peckham on 7/9/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "RewardsTableViewController.h"

@interface RewardsTableViewController ()

@end

@implementation RewardsTableViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
