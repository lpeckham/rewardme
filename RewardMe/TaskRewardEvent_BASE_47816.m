//
//  TaskRewardEvent.m
//  RewardMe
//
//  Created by Linda Peckham on 7/13/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "TaskRewardEvent.h"
#import "TaskReward.h"


@implementation TaskRewardEvent

@dynamic dateTimeOfEvent;
@dynamic rewardValue;
@dynamic taskReward;

@end
