//
//  RewardDetail.m
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "RewardDetail.h"
#import "Reward.h"


@implementation RewardDetail

@dynamic pointsRequiredForReward;
@dynamic detailOfReward;
@dynamic isRepeatable;
@dynamic rewards;

@end
