//
//  TaskReward.h
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//


@class TaskDetail, TaskRewardEvent;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) TaskDetail *details;
@property (nonatomic, retain) NSSet *events;
@end

@interface Task (CoreDataGeneratedAccessors)

- (void)addEventsObject:(TaskRewardEvent *)value;
- (void)removeEventsObject:(TaskRewardEvent *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

@end
