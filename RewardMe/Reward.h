//
//  Reward.h
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//


@class NSManagedObject, TaskRewardEvent;

@interface Reward : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSManagedObject *details;
@property (nonatomic, retain) NSSet *events;
@end

@interface Reward (CoreDataGeneratedAccessors)

- (void)addEventsObject:(TaskRewardEvent *)value;
- (void)removeEventsObject:(TaskRewardEvent *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

@end
