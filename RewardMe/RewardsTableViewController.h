//
//  SecondViewController.h
//  RewardMe
//
//  Created by Linda Peckham on 7/9/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//


#import "Reward.h"
#import "RewardDetail.h"
#import "TaskRewardEvent.h"

@interface RewardsTableViewController : UIViewController

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end

