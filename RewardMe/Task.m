//
//  TaskReward.m
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "Task.h"
#import "TaskDetail.h"
#import "TaskRewardEvent.h"


@implementation Task

@dynamic name;
@dynamic details;
@dynamic events;

@end
