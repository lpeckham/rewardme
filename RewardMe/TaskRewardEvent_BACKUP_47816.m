//
//  TaskRewardEvent.m
//  RewardMe
//
//  Created by Linda Peckham on 7/14/15.
//  Copyright (c) 2015 Linda Peckham. All rights reserved.
//

#import "TaskRewardEvent.h"
#import "Task.h"


@implementation TaskRewardEvent

@dynamic dateTimeOfEvent;
@dynamic rewardValue;
@dynamic isATask;
@dynamic eventTask;
@dynamic eventReward;

@end
