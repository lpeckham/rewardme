# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This repository holds the code for an iOS8 project named "RewardMe!". It is a variation on a task manager app. The main difference is rather than simply managing tasks, it lets the user to claim a reward when so many points have been earned doing tasks.

* Version

0.1 alpha

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
   Download full project, and run it in XCode6 or later.

* Configuration
   The project uses the 'Magic Record' libraries. Those libraries are included in the project.

* Dependencies
   MagicalRecord  (CocoaPod)
   CoreData
   UIKit
   Foundation

* Deployment instructions
  This is currently in alpha development, and so must be run by the simulator, or downloaded to an iPhone with iOS 8.0 or greater, using the USB connection.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact